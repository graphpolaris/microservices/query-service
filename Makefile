develop:
	clear
	bun install
	bun run dev

setup:
	cp .env.example .env
	bun link ts-common
	bun install
	bun link