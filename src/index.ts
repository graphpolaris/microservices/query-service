import { RabbitMqBroker } from 'ts-common/rabbitMq';
import { rabbitMq, redis } from './variables';
import { log } from './logger';
import { queryServiceReader } from './readers/queryService';
import { insightProcessor } from './readers/insightProcessor';

async function main() {
  log.info('Starting query-service...');

  log.info('Connecting to RabbitMQ...');
  const frontendPublisher = await new RabbitMqBroker(rabbitMq, 'ui-direct-exchange').connect();
  const mlPublisher = await new RabbitMqBroker(
    rabbitMq,
    'ml-direct-exchange',
    'query-service',
    undefined,
    { autoDelete: false },
    { autoDelete: false },
  ).connect();
  log.info('Connected to RabbitMQ!');

  log.info('Connecting to Redis...');
  await redis.connect();
  log.info('Connected to Redis!');

  await queryServiceReader(frontendPublisher, mlPublisher, 'neo4j');
  await insightProcessor();
}

await main();
