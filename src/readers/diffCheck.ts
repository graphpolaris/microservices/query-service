import type { SaveState } from 'ts-common';
import { log } from '../logger';
import { hashDictionary, hashIsEqual } from '../utils/hashing';
import type { GraphQueryResultMetaFromBackend } from 'ts-common/src/model/webSocket/graphResult';
import { ums } from '../variables';
import type { InsightModel } from 'ts-common';

export const diffCheck = async (
  insight: InsightModel,
  ss: SaveState,
  graphResult: GraphQueryResultMetaFromBackend,
): Promise<InsightModel> => {
  const previousQueryResult = await ums.getInsightHash(insight.id);

  log.debug('Received query request:', ss);

  const queryResultHash = hashDictionary({
    nodes: graphResult.nodes.map(node => node._id),
    edges: graphResult.edges.map(edge => edge._id),
  });

  log.debug('Comparing hash values from current and previous query');
  const changed = !previousQueryResult || !hashIsEqual(queryResultHash, previousQueryResult);
  insight.status ||= changed;
  log.debug('Updated node and edge ids in SaveState');

  if (changed) {
    await ums.setInsightHash(insight.id, queryResultHash);
    log.debug('Saved new hash value to the database');
  }

  return insight;
};
