import { rabbitMq, ums, mail, SMTP_USER, DEBUG_EMAIL } from '../variables';
import { log } from '../logger';
import { type InsightModel } from 'ts-common';
import { createHeadlessEditor } from '@lexical/headless';
import { $generateHtmlFromNodes } from '@lexical/html';
import { JSDOM } from 'jsdom';
import { Query2BackendQuery } from '../utils/reactflow/query2backend';
import { query2Cypher } from '../utils/cypher/converter';
import { queryService } from './queryService';
import { statCheck } from './statCheck';
import { diffCheck } from './diffCheck';
import { VariableNode } from '../utils/lexical';
import { populateTemplate } from '../utils/insights';
import { RabbitMqBroker } from 'ts-common/rabbitMq';

const dom = new JSDOM();
function setUpDom() {
  const _window = global.window;
  const _document = global.document;
  const _documentFragment = global.DocumentFragment;
  const _navigator = global.navigator;

  // @ts-expect-error - global.window is readonly
  global.window = dom.window;
  global.document = dom.window.document;
  global.DocumentFragment = dom.window.DocumentFragment;
  global.navigator = dom.window.navigator;

  return () => {
    global.window = _window;
    global.document = _document;
    global.DocumentFragment = _documentFragment;
    global.navigator = _navigator;
  };
}

export const insightProcessor = async () => {
  if (mail == null) {
    log.warn('Mail is not configured. Insight processor will be disabled');
    return;
  }

  log.info('Starting insight processor');

  const insightProcessorConsumer = await new RabbitMqBroker(
    rabbitMq,
    'insight-processor',
    `insight-processor`,
    `insight-processor`,
  ).connect();
  log.info('Connected to RabbitMQ ST!');

  await insightProcessorConsumer.startConsuming<{ insight: InsightModel; force: boolean }>('query-service', async (message, headers) => {
    let insight = message.insight;
    if (insight == null || insight.template == null || insight.userId == null || insight.saveStateId == null) {
      log.error('Invalid Insight received in insightProcessorConsumer:', insight);
      return;
    }

    if (insight.alarmMode === 'disabled' && !message.force) {
      log.debug('Alarm mode is disabled', insight.id);
      return;
    }

    if (insight.recipients == null || insight.recipients.length === 0) {
      log.debug('No recipients found in the insight, skipping');
      return;
    }

    log.info('Received insight to be processed', insight);

    const editor = createHeadlessEditor({
      nodes: [VariableNode],
      onError: error => {
        log.error(error);
      },
    });

    editor.update(() => {
      const state = editor.parseEditorState(JSON.parse(insight.template));
      editor.setEditorState(state);
    });

    if (insight.userId == null) return;
    const ss = await ums.getUserSaveState(insight.userId, insight.saveStateId);

    const queries = ss.queryStates.openQueryArray;
    const visualizations = ss.visualizations.openVisualizationArray;

    for (const queryIndex in queries) {
      const visualQuery = ss.queryStates.openQueryArray[queryIndex].graph;
      const queryBuilderSettings = ss.queryStates.openQueryArray[queryIndex].settings;
      const convertedQuery = Query2BackendQuery(ss.id, visualQuery, queryBuilderSettings, []);
      const cypher = query2Cypher(convertedQuery);
      if (cypher == null) return;
      try {
        const result = await queryService(ss.dbConnections[0], cypher, true);

        insight.status = false;

        if (insight.alarmMode === 'always') {
          insight.status = true;
        } else if (insight.alarmMode === 'diff') {
          insight = await diffCheck(insight, ss, result);
        } else if (insight.alarmMode === 'conditional' && insight.conditionsCheck && insight.conditionsCheck.length > 0) {
          insight = statCheck(insight, result);
        }

        if (insight.userId == null) return; // fixes ts but never is the case
        await ums.updateInsight(insight.userId, insight.id, insight);

        if (insight.status || message.force) {
          if (insight.status) log.debug('Insight passed the check');
          if (message.force) log.debug('Forced insight processing');

          editor.read(async () => {
            const cleanUpDom = setUpDom();
            let html = $generateHtmlFromNodes(editor);
            cleanUpDom();

            html = await populateTemplate(html, result, visualizations);

            for (const recipient of insight.recipients) {
              if (mail == null) {
                log.warn('Mail is not configured. Insight processor will be disabled');
                return;
              }

              if (DEBUG_EMAIL) {
                log.warn('DEBUG: Would have sent mail to', recipient);
                continue;
              }

              log.debug('Sending mail to', recipient);
              await mail.sendMail({
                to: recipient,
                from: SMTP_USER,
                subject: `GraphPolaris report: ${insight.name}`,
                html: html,
              });
              log.info('Mail sent to ', recipient);
            }
          });
        } else {
          log.debug('WARN: Insight did not pass the check');
        }
      } catch (err) {
        log.error('Error processing insight', err);
      }
    }
  });
};
