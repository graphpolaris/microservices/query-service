import type { GraphQueryResultMetaFromBackend, InsightModel } from 'ts-common';
import { log } from '../logger';

function processAlarmStats(alarmStat: InsightModel, resultQuery: GraphQueryResultMetaFromBackend): boolean {
  for (const condition of alarmStat.conditionsCheck) {
    const ssInsightNode = condition.nodeLabel;
    const ssInsightStatistic = condition.statistic;
    const ssInsightOperator = condition.operator;
    const ssInsightValue = condition.value;
    // TODO: eventually we need to support multiple conditions with and/or

    log.debug(`Checking condition: ${ssInsightNode} ${ssInsightStatistic} ${ssInsightOperator} ${ssInsightValue}`);

    if (resultQuery.metaData.nodes.labels.includes(ssInsightNode)) {
      const nodeCount = resultQuery.metaData.nodes.count;
      let conditionMet = false;
      switch (ssInsightOperator) {
        case '>':
          conditionMet = nodeCount > ssInsightValue;
          break;
        case '>=':
          conditionMet = nodeCount >= ssInsightValue;
          break;
        case '==':
          conditionMet = nodeCount === ssInsightValue;
          break;
        case '!=':
          conditionMet = nodeCount !== ssInsightValue;
          break;
        case '<':
          conditionMet = nodeCount < ssInsightValue;
          break;
        case '<=':
          conditionMet = nodeCount <= ssInsightValue;
          break;
        default:
          log.error(`Unsupported operator: ${ssInsightOperator}`);
          throw new Error(`Unsupported operator: ${ssInsightOperator}`);
      }

      if (conditionMet) {
        log.info('Condition met ');
        return true;
      } else {
        log.info('Condition not met');
        return false;
      }
    }

    break; // TODO: only one condition is supported for now
  }

  return false;
}

export const statCheck = (insight: InsightModel, graphResult: GraphQueryResultMetaFromBackend): InsightModel => {
  if (insight.type === 'report') {
    // report is not an alarm, just log the stats
    log.debug('report!', insight);
    return insight;
  }

  insight.status ||= processAlarmStats(insight, graphResult); //statusResult;
  log.debug('Alarm status', insight.status);

  return insight;
};
