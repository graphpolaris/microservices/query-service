# Explanation

This folder contains the Go client for ArangoDB. It is used to interact with the ArangoDB database. Since it is not tested or used currently, I decided not to port it for now (until we can test it properly).
