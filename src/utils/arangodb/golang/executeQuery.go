/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package arangodb

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	entityarangodb "git.science.uu.nl/graphpolaris/query-execution/usecases/arangodb/entity"
	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
)

/*
ExecuteQuery executes the given query on an ArangoDB instance

	query: string, the query to be executed
	databaseUsername: string
	databasePassword: string
	databaseHost: string, URL for the database
	databasePort: int
	internalDatabase: string, the internal database to use, not all databases require this
	Return: (*[]byte, error), returns the result of the query and a potential error
*/
func (s *Service) ExecuteQuery(query string, dbConnection *models.DBConnectionModel) (*[]byte, error) {

	// Create data structure to store query result in
	var queryResult = make(map[string][]entityarangodb.Document)

	// Open connection to ArangoDB
	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{fmt.Sprintf("%s:%d", dbConnection.URL, dbConnection.Port)}, // FIXME missing protocol!
		TLSConfig: &tls.Config{InsecureSkipVerify: true},
	})
	if err != nil {
		return nil, err
	}

	// Create a new driver client
	c, err := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(dbConnection.Username, dbConnection.Password),
	})
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	db, err := c.Database(ctx, dbConnection.InternalDatabaseName)
	if err != nil {
		return nil, err
	}

	cursor, err := db.Query(ctx, query, nil)
	if err != nil {
		return nil, err
	}
	defer cursor.Close()

	// Loop through the resulting documents
	listContainer := entityarangodb.ListContainer{}
	listContainer.EdgeList = make([]entityarangodb.Document, 0)
	listContainer.NodeList = make([]entityarangodb.Document, 0)
	listContainer.RowList = make([]entityarangodb.Document, 0)
	num := -1.0
	isNum := false

	for {
		var doc interface{}
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {

			// Handle other errors
			return nil, err
		}

		// Switch on the type of return, to filter out the case of a single number
		switch doc.(type) {
		case float64:
			num = doc.(float64)
			isNum = true
			break
		case map[string]interface{}:
			pdoc := doc.(map[string]interface{})
			parseResult(pdoc, &listContainer)
			break
		default:
			fmt.Println("Incompatible result type")
			break
		}
	}

	if !isNum {
		// Return nodes and edges
		queryResult["nodes"] = listContainer.NodeList
		queryResult["edges"] = listContainer.EdgeList
		queryResult["rows"] = listContainer.RowList
		jsonQ, err := json.Marshal(queryResult)
		return &jsonQ, err
	}
	// Return just a number
	numQ, err := json.Marshal(num)
	return &numQ, err
}

/*
parseResult takes the result of the query and translates this to two lists: a nodelist and an edgelist, stored in a listcontainer

	doc: map[string]interface{}, the document with the nodes and vertices that came back from the database
	listContainer: *entityarangodb.ListContainer, a struct containing the nodelist and edgelist that will be returned to the frontend
*/
func parseResult(doc map[string]interface{}, listContainer *entityarangodb.ListContainer) {
	var vertices []interface{}
	var edges []interface{}
	_, vertex := doc["vertices"]
	_, edge := doc["edges"]
	if vertex && edge { // Node link
		vertices = doc["vertices"].([]interface{})
		edges = doc["edges"].([]interface{})
	} else { // Table
		(*listContainer).RowList = append(((*listContainer).RowList), doc)
	}

	for _, vertex := range vertices {
		if vertex != nil {
			vertexDoc := vertex.(map[string]interface{})

			(*listContainer).NodeList = append((*listContainer).NodeList, parseNode(vertexDoc))
		}
	}

	for _, edge := range edges {
		if edge != nil {
			edgeDoc := edge.(map[string]interface{})

			(*listContainer).EdgeList = append((*listContainer).EdgeList, parseEdge(edgeDoc))
		}
	}
}

/*
parseEdge parses the data of an edge to an output-friendly format

	doc: map[string]interface{}, a single edge returned from the database
	Returns: entityarangodb.Document, a document with almost the same structure as before, but the attributes are grouped
*/
func parseEdge(doc map[string]interface{}) entityarangodb.Document {
	data := make(entityarangodb.Document)
	data["id"] = doc["_id"]
	delete(doc, "_id")
	delete(doc, "_key")
	delete(doc, "_rev")
	data["from"] = doc["_from"]
	delete(doc, "_from")
	data["to"] = doc["_to"]
	delete(doc, "_to")
	data["attributes"] = doc

	return data
}

/*
parseEdge parses the data of a node to an output-friendly format

	doc: map[string]interface{}, a single entry of an node
	Returns: entityarangodb.Document, a document with almost the same structure as before, but the attributes are grouped
*/
func parseNode(doc map[string]interface{}) entityarangodb.Document {
	data := make(entityarangodb.Document)
	data["id"] = doc["_id"]
	delete(doc, "_id")
	delete(doc, "_key")
	delete(doc, "_rev")
	data["attributes"] = doc

	return data
}
