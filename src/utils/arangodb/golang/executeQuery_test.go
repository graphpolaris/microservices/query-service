/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package arangodb

import (
	"testing"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"github.com/arangodb/go-driver"
	"github.com/stretchr/testify/assert"
)

var s *Service

/*
Creates the service
*/
func createService() {
	if s != nil {
		return
	}
	s = NewService()
}

/*
Test to make sure that using invalid credentials does not crash the service

	t: *testing.T, makes go recognize this as a test
*/
func TestInvalidCredentials(t *testing.T) {
	createService()
	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	_, err := s.ExecuteQuery("", connection)
	assert.Error(t, err)
}

/*
Test to make sure an invalid port does not crash the service

	t: *testing.T, makes go recognize this as a test
*/
func TestInvalidPort(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 3000,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	_, err := s.ExecuteQuery("", connection)
	assert.Error(t, err)
}

/*
Test to make sure an invalid url does not crash the service

	t: *testing.T, makes go recognize this as a test
*/
func TestInvalidURL(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhostt",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	_, err := s.ExecuteQuery("", connection)
	assert.Error(t, err)
}

/*
Test to make sure an empty query does not crash the service

	t: *testing.T, makes go recognize this as a test
*/
func TestEmptyQuery(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	_, err := s.ExecuteQuery("", connection)

	// Assert we get an 'empty query' error
	assert.Equal(t, 1502, err.(driver.ArangoError).ErrorNum)
}

/*
Test to make sure a different database than '_system' can be used

	t: *testing.T, makes go recognize this as a test
*/
func TestDifferentInternalDatabase(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "development",
	}
	_, err := s.ExecuteQuery("", connection)

	// Assert we get an 'empty query' error
	assert.Equal(t, 1502, err.(driver.ArangoError).ErrorNum)
}

/*
Test a few different valid queries

	t: *testing.T, makes go recognize this as a test
*/
func TestValidQueries(t *testing.T) {
	createService()

	var queryTests = []struct {
		name string
		in   string
		out  string
	}{
		{
			name: `only nodes query`,
			in: `LET n0 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			LET nodes = first(RETURN UNION_DISTINCT(n0,[],[]))
			LET edges = first(RETURN UNION_DISTINCT([],[]))
			RETURN {"vertices":nodes, "edges":edges }`,
			out: `{"edges":null,"nodes":[{"_id":"airports/LGA","_key":"LGA","attributes":{"city":"New York","country":"USA","lat":40.77724306,"long":-73.87260917,"name":"LaGuardia","state":"NY","vip":false}},{"_id":"airports/JRB","_key":"JRB","attributes":{"city":"New York","country":"USA","lat":40.70121361,"long":-74.00902833,"name":"Downtown Manhattan/Wall St. Heliport","state":"NY","vip":false}},{"_id":"airports/JRA","_key":"JRA","attributes":{"city":"New York","country":"USA","lat":40.75454583,"long":-74.00708389,"name":"Port Authority-W 30th St Midtown Heliport","state":"NY","vip":false}},{"_id":"airports/JFK","_key":"JFK","attributes":{"city":"New York","country":"USA","lat":40.63975111,"long":-73.77892556,"name":"John F Kennedy Intl","state":"NY","vip":true}},{"_id":"airports/6N7","_key":"6N7","attributes":{"city":"New York","country":"USA","lat":40.73399083,"long":-73.97291639,"name":"New York Skyports Inc. SPB","state":"NY","vip":false}},{"_id":"airports/6N5","_key":"6N5","attributes":{"city":"New York","country":"USA","lat":40.74260167,"long":-73.97208306,"name":"E 34th St Heliport","state":"NY","vip":false}}]}`,
		},
		{
			name: `nodes and edges query`,
			in: `LET n0 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			LET r0 = (FOR x IN n0 FOR v, e, p IN 1..1 OUTBOUND x flights OPTIONS { uniqueEdges: "path" }
			LIMIT 1 RETURN DISTINCT p )
			LET nodes = first(RETURN UNION_DISTINCT(flatten(r0[**].vertices), [],[]))
			LET edges = first(RETURN UNION_DISTINCT(flatten(r0[**].edges), [],[]))
			RETURN {"vertices":nodes, "edges":edges }`,
			out: `{"edges":[{"_from":"airports/JFK","_id":"flights/1837","_key":"1837","_to":"airports/PBI","attributes":{"ArrTime":332,"ArrTimeUTC":"2008-01-01T08:32:00.000Z","Day":1,"DayOfWeek":2,"DepTime":8,"DepTimeUTC":"2008-01-01T05:08:00.000Z","Distance":1028,"FlightNum":859,"Month":1,"TailNum":"N505JB","UniqueCarrier":"B6","Year":2008}}],"nodes":[{"_id":"airports/PBI","_key":"PBI","attributes":{"city":"West Palm Beach","country":"USA","lat":26.68316194,"long":-80.09559417,"name":"Palm Beach International","state":"FL","vip":false}},{"_id":"airports/JFK","_key":"JFK","attributes":{"city":"New York","country":"USA","lat":40.63975111,"long":-73.77892556,"name":"John F Kennedy Intl","state":"NY","vip":true}}]}`,
		},
		{
			name: `node and edge query with two nodes`,
			in: `LET n1 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			LET r0 = (FOR x IN n1 FOR v, e, p IN 1..1 OUTBOUND x flights OPTIONS { uniqueEdges: "path" }
			FILTER v.city == "San Francisco" LIMIT 1 RETURN DISTINCT p )
			LET nodes = first(RETURN UNION_DISTINCT(flatten(r0[**].vertices), [],[]))
			LET edges = first(RETURN UNION_DISTINCT(flatten(r0[**].edges), [],[]))
			RETURN {"vertices":nodes, "edges":edges }`,
			out: `{"edges":[{"_from":"airports/JFK","_id":"flights/1839","_key":"1839","_to":"airports/SFO","attributes":{"ArrTime":327,"ArrTimeUTC":"2008-01-01T11:27:00.000Z","Day":1,"DayOfWeek":2,"DepTime":11,"DepTimeUTC":"2008-01-01T05:11:00.000Z","Distance":2586,"FlightNum":9,"Month":1,"TailNum":"N555UA","UniqueCarrier":"UA","Year":2008}}],"nodes":[{"_id":"airports/SFO","_key":"SFO","attributes":{"city":"San Francisco","country":"USA","lat":37.61900194,"long":-122.3748433,"name":"San Francisco International","state":"CA","vip":true}},{"_id":"airports/JFK","_key":"JFK","attributes":{"city":"New York","country":"USA","lat":40.63975111,"long":-73.77892556,"name":"John F Kennedy Intl","state":"NY","vip":true}}]}`,
		},
		{
			name: `group by query with table return`,
			in: `LET tree_0 = (
				FOR e_9 IN parliament
				LET e10 = (
					FOR e_10 IN commissions
					FOR r8 IN part_of
					FILTER r8._from == e_9._id AND r8._to == e_10._id
					FILTER length(e_10) != 0 AND length(r8) != 0
					RETURN {"e10": union_distinct([e_10], []), "r8": union_distinct([r8], [])}
				)
				FILTER length(e10) != 0 AND length(e_9) != 0
				RETURN {"e9": union_distinct([e_9], []), "e10": union_distinct(flatten(e10[**].e10), []), "r8": union_distinct(flatten(e10[**].r8), [])}
			)
			LET gt13 = (
				FOR x IN part_of
				LET variable_0 = (
					LET tmp = union_distinct(flatten(tree_0[**].e10), [])
						FOR y IN tmp
						FILTER y._id == x._to
						RETURN y._id
				) 
				LET variable_1 = variable_0[0] 
				LET variable_2 = (
					LET tmp = union_distinct(flatten(tree_0[**].e9), [])
						FOR y IN tmp
						FILTER y._id == x._from
						RETURN y.age
				) 
				LET variable_3 = variable_2[0] 
				FILTER variable_1 != NULL AND variable_3 != NULL
				RETURN {
					"group": variable_1, 
					"by": variable_3
				}
			)
			LET g13 = (
				FOR x IN gt13
				COLLECT c = x.group INTO groups = x.by
				LET variable_0 = AVG(groups) 
				FILTER variable_0 > 45  
				RETURN {
				_id: c,
				modifier: variable_0
				}
			)
			FOR x in g13
			RETURN {group: x.modifier, by: x._id}`,
			out: `{"edges":[],"nodes":[],"rows":[{"by":"commissions/15","group":48.53846153846154},{"by":"commissions/20","group":55.333333333333336},
			{"by":"commissions/25","group":45.2},{"by":"commissions/26","group":46.25},{"by":"commissions/27","group":45.25},{"by":"commissions/28","group":55.4},
			{"by":"commissions/29","group":45.36363636363637},{"by":"commissions/3","group":46.4},{"by":"commissions/33","group":45.25},{"by":"commissions/35","group":47},
			{"by":"commissions/36","group":53.75},{"by":"commissions/4","group":46.15384615384615}]}`,
		},
	}

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	for _, tt := range queryTests {
		t.Run(tt.name, func(t *testing.T) {
			r, _ := s.ExecuteQuery(tt.in, connection)
			assert.Equal(t, tt.out, string(*r))
		})
	}
}

/*
Test to validate the query on ther internal database

	t: *testing.T, makes go recognize this as a test
*/
func TestValidQueriesOnOtherInternalDatabase(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "development",
	}
	var queryTests = []struct {
		name string
		in   string
		out  string
	}{
		{
			name: `only nodes query`,
			in: `LET n0 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			LET nodes = first(RETURN UNION_DISTINCT(n0,[],[]))
			LET edges = first(RETURN UNION_DISTINCT([],[]))
			RETURN {"vertices":nodes, "edges":edges }`,
			out: `{"edges":null,"nodes":[{"_id":"airports/LGA","_key":"LGA","attributes":{"city":"New York","country":"USA","lat":40.77724306,"long":-73.87260917,"name":"LaGuardia","state":"NY","vip":false}},{"_id":"airports/JRB","_key":"JRB","attributes":{"city":"New York","country":"USA","lat":40.70121361,"long":-74.00902833,"name":"Downtown Manhattan/Wall St. Heliport","state":"NY","vip":false}},{"_id":"airports/JRA","_key":"JRA","attributes":{"city":"New York","country":"USA","lat":40.75454583,"long":-74.00708389,"name":"Port Authority-W 30th St Midtown Heliport","state":"NY","vip":false}},{"_id":"airports/JFK","_key":"JFK","attributes":{"city":"New York","country":"USA","lat":40.63975111,"long":-73.77892556,"name":"John F Kennedy Intl","state":"NY","vip":true}},{"_id":"airports/6N7","_key":"6N7","attributes":{"city":"New York","country":"USA","lat":40.73399083,"long":-73.97291639,"name":"New York Skyports Inc. SPB","state":"NY","vip":false}},{"_id":"airports/6N5","_key":"6N5","attributes":{"city":"New York","country":"USA","lat":40.74260167,"long":-73.97208306,"name":"E 34th St Heliport","state":"NY","vip":false}}]}`,
		},
	}

	for _, tt := range queryTests {
		t.Run(tt.name, func(t *testing.T) {
			r, _ := s.ExecuteQuery(tt.in, connection)
			assert.Equal(t, tt.out, string(*r))
		})
	}
}

/*
Test queries that return a single number

	t: *testing.T, makes go recognize this as a test
*/
func TestCountQueries(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	var queryTests = []struct {
		name string
		in   string
		out  string
	}{
		{
			name: `cities in new york`,
			in: `LET n0 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			RETURN LENGTH (n0)`,
			out: `6`,
		},
		{
			name: `flights from new york`,
			in: `LET n0 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			LET r0 = (FOR x IN n0 FOR v, e, p IN 1..1 OUTBOUND x flights OPTIONS { uniqueEdges: "path" }
			RETURN DISTINCT p )
			RETURN LENGTH (unique(r0[*].edges[**]))`,
			out: `9421`,
		},
	}

	for _, tt := range queryTests {
		t.Run(tt.name, func(t *testing.T) {
			r, _ := s.ExecuteQuery(tt.in, connection)
			assert.Equal(t, tt.out, string(*r))
		})
	}
}

/*
Test some bad queries and make sure they do not crash the service

	t: *testing.T, makes go recognize this as a test
*/
func TestInvalidQueries(t *testing.T) {
	createService()

	connection := &models.DBConnectionModel{
		URL:                  "http://localhost",
		Username:             "root",
		Password:             "root",
		Port:                 8529,
		Protocol:             "http",
		InternalDatabaseName: "_system",
	}
	var queryTests = []struct {
		name string
		in   string
		out  string
	}{
		{
			name: `flights from new york`,
			in: `LET n0 = (FOR x IN airports FILTER x.city == "New York" RETURN x)
			LET r0 = (FOR x IN n0 FOR v, e, p IN 1..1 OUTBOUND x flights OPTIONS { uniqueEdges: "pat\" }
			RETURN DISTINCT p )
			RETURN LENGTH (unique(r0[*].edges[**]))`,
			out: `9421`,
		},
	}

	for _, tt := range queryTests {
		t.Run(tt.name, func(t *testing.T) {
			r, err := s.ExecuteQuery(tt.in, connection)

			// Assert that we got no result
			assert.Nil(t, r)

			// Assert that the error we got has code 1501 (invalid query)
			assert.Equal(t, 1501, err.(driver.ArangoError).ErrorNum)
		})
	}
}
