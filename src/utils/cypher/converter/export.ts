import type { ExportNodeStruct, NodeStruct } from 'ts-common';

export function createExportCypher(JSONQuery: ExportNodeStruct, id: string): string {
  return `${id}.${JSONQuery.attribute} as ${JSONQuery.id}`;
}

export function extractExportCypher(JSONQuery: NodeStruct): string[] {
  const exports: string[] = [];
  if (JSONQuery.export) {
    for (const exportItem of JSONQuery.export) {
      if (exportItem && JSONQuery.id) {
        const exportCypher = createExportCypher(exportItem, JSONQuery.id);
        exports.push(exportCypher);
      }
    }
  }

  if (JSONQuery.relation && JSONQuery.relation.node) {
    const relationCypher = extractExportCypher(JSONQuery.relation.node);
    exports.push(...relationCypher);
  }
  return [...new Set(exports)];
}
