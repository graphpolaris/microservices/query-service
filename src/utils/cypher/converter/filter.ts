import type { FilterStruct } from 'ts-common';

export function createFilterCypher(JSONQuery: FilterStruct, ID: string): string {
  let cypher = `${ID}.${JSONQuery.attribute}`;
  switch (JSONQuery.operation) {
    case 'EQ':
      cypher += ` = ${JSONQuery.value}`;
      break;
    case 'NEQ':
      cypher += ` <> ${JSONQuery.value}`;
      break;
    case 'GT':
      cypher += ` > ${JSONQuery.value}`;
      break;
    case 'GTE':
      cypher += ` >= ${JSONQuery.value}`;
      break;
    case 'LT':
      cypher += ` < ${JSONQuery.value}`;
      break;
    case 'LTE':
      cypher += ` <= ${JSONQuery.value}`;
      break;
    case 'CONTAINS':
      cypher += ` CONTAINS ${JSONQuery.value}`;
      break;
    case 'STARTS_WITH':
      cypher += ` STARTS WITH ${JSONQuery.value}`;
      break;
    case 'ENDS_WITH':
      cypher += ` ENDS WITH ${JSONQuery.value}`;
      break;
    case 'IN':
      cypher += ` IN ${JSONQuery.value}`;
      break;
    case 'NOT_IN':
      cypher += ` NOT IN ${JSONQuery.value}`;
      break;
    case 'IS_NULL':
      cypher += ` IS NULL`;
      break;
    case 'IS_NOT_NULL':
      cypher += ` IS NOT NULL`;
      break;
    default:
      throw new Error('operator not supported');
  }
  return cypher;
}
