import { StringFilterTypes, type AnyStatement } from 'ts-common/src/model/query/logic/general';
import type { QueryCacheData } from './model';
import { log } from 'ts-common/src/logger/logger';

export function createWhereLogic(
  op: string,
  left: string,
  whereLogic: Set<string>,
  cacheData: QueryCacheData,
): { logic: string; where: Set<string> } {
  const newWhereLogic = `${left.replace('.', '_')}_${op}`;

  // TODO: Relation temporarily ignored due to unnecessary added complexity in the query
  // for (const relation of cacheData.relations) {
  //     if (relation.id !== left) {
  //         remainingNodes.add(relation.id);
  //     }
  // }

  whereLogic.add(`${op}(${left}) AS ${newWhereLogic}`);
  for (const entity of cacheData.entities) {
    if (entity.id !== left) {
      whereLogic.add(entity.id);
    }
  }

  return { logic: newWhereLogic, where: whereLogic };
}

export function extractLogicCypher(logicQuery: AnyStatement, cacheData: QueryCacheData): { logic: string; where: Set<string> } {
  switch (typeof logicQuery) {
    case 'object':
      if (Array.isArray(logicQuery)) {
        let op = logicQuery[0].replace('_', '').toLowerCase();
        let right = logicQuery?.[2];
        const { logic: left, where: whereLogic } = extractLogicCypher(logicQuery[1], cacheData);

        switch (op) {
          case '!=':
            op = '<>';
            break;
          case '==':
            if (right === '".*"') op = '=~'; // in case "equals to <empty string>", this is what happens
            else op = '=';
            break;
          case 'like':
            op = '=~';
            break;
          case 'in':
          case 'present in list':
            op = 'IN';
            if (typeof right === 'string') {
              return {
                logic: `(${left} IN [${right
                  .replace(/"/g, '')
                  .split(',')
                  .map((r: string) => `"${r.trim()}"`)
                  .join(', ')}])`,
                where: whereLogic,
              };
            }
            break;
          case 'not in':
          case 'not in list':
            op = 'NOT IN';
            if (typeof right === 'string') {
              return {
                logic: `(NOT ${left} IN [${right
                  .replace(/"/g, '')
                  .split(',')
                  .map((r: string) => `"${r.trim()}"`)
                  .join(', ')}])`,
                where: whereLogic,
              };
            }
            break;
          case 'isempty':
            return { logic: `(${left} IS NULL OR ${left} = "")`, where: whereLogic };
          case 'isnotempty':
            return { logic: `(${left} IS NOT NULL AND ${left} <> "")`, where: whereLogic };
          case 'lower':
            return { logic: `toLower(${left})`, where: whereLogic };
          case 'upper':
            return { logic: `toUpper(${left})`, where: whereLogic };
          case 'avg':
          case 'count':
          case 'max':
          case 'min':
          case 'sum':
            return createWhereLogic(op, left, whereLogic, cacheData);
        }
        if (logicQuery.length > 2 && logicQuery[2]) {
          const { logic: rightLogic, where: whereLogicRight } = extractLogicCypher(logicQuery[2], cacheData);
          let rightMutable = rightLogic;

          for (const where of whereLogicRight) {
            whereLogic.add(where);
          }
          if (op === '=~') {
            rightMutable = `(".*" + ${rightMutable} + ".*")`;
          }
          return { logic: `(${left} ${op} ${rightMutable})`, where: whereLogic };
        }
        return { logic: `(${op} ${left})`, where: whereLogic };
      }
      return { logic: logicQuery, where: new Set() };
    case 'string':
      return { logic: logicQuery.replace('@', ''), where: new Set() };
    case 'number':
      return { logic: logicQuery.toString(), where: new Set() };
    default:
      return { logic: logicQuery as any, where: new Set() };
  }
}
