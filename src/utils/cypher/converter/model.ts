export interface RelationCacheData {
  id: string;
  queryId: string;
}

export interface EntityCacheData {
  id: string;
  queryId: string;
}

export interface QueryCacheData {
  entities: EntityCacheData[];
  relations: RelationCacheData[];
  unwinds: string[];
}
