import type { NodeStruct } from 'ts-common';
import type { QueryCacheData } from './model';
import { getRelationCypher } from './relation';

export function getNodeCypher(JSONQuery: NodeStruct, cacheData: QueryCacheData): [string, QueryCacheData] {
  let label = '';
  if (JSONQuery.label) {
    label = `:${JSONQuery.label}`;
  }
  let id = '';
  if (JSONQuery.id) {
    id = JSONQuery.id;
  }

  if (id) {
    cacheData.entities.push({ id, queryId: '' });
  }

  let cypher = `(${id}${label})`;

  if (JSONQuery.relation) {
    const [relationCypher, _cacheData] = getRelationCypher(JSONQuery.relation, cacheData);

    if (relationCypher) {
      if (JSONQuery.relation.direction === 'FROM') {
        cypher += `<-${relationCypher}`;
      } else if (JSONQuery.relation.direction === 'TO') {
        cypher += `-${relationCypher}`;
      } else {
        cypher += `-${relationCypher}`;
      }
    }
    return [cypher, _cacheData];
  }
  return [cypher, cacheData];
}
