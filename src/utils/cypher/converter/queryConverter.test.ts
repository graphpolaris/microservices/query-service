import { query2Cypher } from './queryConverter';
import { StringFilterTypes, type BackendQueryFormat } from 'ts-common';
import { expect, test, describe, it } from 'bun:test';

function fixCypherSpaces(cypher?: string | null): string {
  if (!cypher) {
    return '';
  }

  let trimmedCypher = cypher.replace(/\n/g, ' ');
  trimmedCypher = trimmedCypher.replaceAll(/ {2,50}/g, ' ');
  trimmedCypher = trimmedCypher.replace(/\t+/g, '');
  return trimmedCypher.trim();
}

describe('query2Cypher', () => {
  it('should return correctly on a simple query with multiple paths', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      return: ['*'],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              label: 'DIRECTED',
              direction: 'TO',
              depth: { min: 1, max: 1 },
              node: {
                label: 'Movie',
                id: 'm1',
              },
            },
          },
        },
        {
          id: 'path2',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              label: 'IN_GENRE',
              direction: 'TO',
              depth: { min: 1, max: 1 },
              node: {
                label: 'Genre',
                id: 'g1',
              },
            },
          },
        },
      ],
      limit: 5000,
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person)-[:DIRECTED*1..1]->(m1:Movie))
        MATCH path2 = ((p1:Person)-[:IN_GENRE*1..1]->(g1:Genre))
        RETURN * LIMIT 5000`;
    const expectedCypherCount = `MATCH path1 = ((p1:Person)-[:DIRECTED*1..1]->(m1:Movie))
        MATCH path2 = ((p1:Person)-[:IN_GENRE*1..1]->(g1:Genre))
        RETURN COUNT(DISTINCT p1) as p1_count, COUNT(DISTINCT m1) as m1_count, COUNT(DISTINCT g1) as g1_count`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
    expect(fixCypherSpaces(cypher.countQuery)).toBe(fixCypherSpaces(expectedCypherCount));
  });

  it('should return correctly on a complex query with logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      return: ['*'],
      logic: ['!=', '@p1.name', '"Raymond Campbell"'],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              label: 'DIRECTED',
              direction: 'TO',
              depth: { min: 1, max: 1 },
              node: {
                label: 'Movie',
                id: 'm1',
              },
            },
          },
        },
        {
          id: 'path2',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              label: 'IN_GENRE',
              direction: 'TO',
              depth: { min: 1, max: 1 },
              node: {
                label: 'Genre',
                id: 'g1',
              },
            },
          },
        },
      ],
      limit: 5000,
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person)-[:DIRECTED*1..1]->(m1:Movie))
        MATCH path2 = ((p1:Person)-[:IN_GENRE*1..1]->(g1:Genre)) 
        WHERE (p1.name <> "Raymond Campbell")
        RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with group by logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 5000,
      logic: ['And', ['<', '@movie.imdbRating', 7.5], ['==', 'p2.age', 'p1.age']],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              id: 'acted',
              label: 'ACTED_IN',
              depth: { min: 1, max: 1 },
              direction: 'TO',
              node: {
                label: 'Movie',
                id: 'movie',
              },
            },
          },
        },
        {
          id: 'path2',
          node: {
            label: 'Person',
            id: 'p2',
          },
        },
      ],
      return: ['@path2'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
        MATCH path2 = ((p2:Person)) 
        WHERE ((movie.imdbRating < 7.5) and (p2.age = p1.age))
        RETURN path2 LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with no label', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 5000,
      logic: ['<', ['-', '@movie.year', 'p1.year'], 10],
      query: [
        {
          id: 'path1',
          node: {
            id: 'p1',
            filter: [],
            relation: {
              id: 'acted',
              depth: { min: 1, max: 1 },
              direction: 'TO',
              node: {
                label: 'Movie',
                id: 'movie',
              },
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1)-[acted*1..1]->(movie:Movie))
        WHERE ((movie.year - p1.year) < 10)
        RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with no depth', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 5000,
      logic: ['And', ['<', '@movie.imdbRating', 7.5], ['==', 'p2.age', 'p1.age']],
      query: [
        {
          id: 'path1',
          node: {
            id: 'p1',
            relation: {
              id: 'acted',
              direction: 'TO',
              node: {
                label: 'Movie',
                id: 'movie',
              },
            },
          },
        },
        {
          id: 'path2',
          node: {
            id: 'p2',
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1)-[acted]->(movie:Movie))
        MATCH path2 = ((p2)) 
        WHERE ((movie.imdbRating < 7.5) and (p2.age = p1.age))
        RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with average calculation', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 5000,
      logic: ['<', '@p1.age', ['Avg', '@p1.age']],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              id: 'acted',
              label: 'ACTED_IN',
              depth: { min: 1, max: 1 },
              direction: 'TO',
              node: {
                label: 'Movie',
                id: 'movie',
              },
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
        WITH avg(p1.age) AS p1_age_avg, p1, movie
        MATCH path1 = ((p1:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
        WHERE (p1.age <  p1_age_avg)
        RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with average calculation and multiple paths', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 5000,
      logic: ['<', '@p1.age', ['Avg', '@p1.age']],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              id: 'acted',
              label: 'ACTED_IN',
              depth: { min: 1, max: 1 },
              direction: 'TO',
              node: {
                label: 'Movie',
                id: 'movie',
              },
            },
          },
        },
        {
          id: 'path2',
          node: {
            label: 'Person',
            id: 'p2',
            relation: {
              id: 'acted',
              label: 'ACTED_IN',
              depth: { min: 1, max: 1 },
              direction: 'TO',
              node: {
                label: 'Movie',
                id: 'movie',
              },
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
      MATCH path2 = ((p2:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
      WITH avg(p1.age) AS p1_age_avg, p1, movie, p2
      MATCH path1 = ((p1:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
      MATCH path2 = ((p2:Person)-[acted:ACTED_IN*1..1]->(movie:Movie))
      WHERE (p1.age < p1_age_avg) RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a single entity query with lower like logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 5000,
      logic: ['Like', ['Lower', '@p1.name'], '"john"'],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person))
        WHERE (toLower(p1.name) =~ (".*" + "john" + ".*"))
        RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with like logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      logic: ['Like', '@id_1691576718400.title', '"ale"'],
      query: [
        {
          id: 'path_0',
          node: {
            id: 'id_1691576718400',
            label: 'Employee',
            relation: {
              id: 'id_1691576720177',
              label: 'REPORTS_TO',
              direction: 'TO',
              node: {},
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path_0 = ((id_1691576718400:Employee)-[id_1691576720177:REPORTS_TO]->()) 
        WHERE (id_1691576718400.title =~ (".*" + "ale" + ".*")) 
        RETURN * LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with both direction relation', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      logic: ['Like', '@id_1691576718400.title', '"ale"'],
      query: [
        {
          id: 'path_0',
          node: {
            id: 'id_1691576718400',
            label: 'Employee',
            relation: {
              id: 'id_1691576720177',
              label: 'REPORTS_TO',
              direction: 'BOTH',
              node: {},
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path_0 = ((id_1691576718400:Employee)-[id_1691576720177:REPORTS_TO]-()) 
        WHERE (id_1691576718400.title =~ (".*" + "ale" + ".*")) 
        RETURN * LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with relation logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      logic: ['<', '@id_1698231933579.unitPrice', '10'],
      query: [
        {
          id: 'path_0',
          node: {
            relation: {
              id: 'id_1698231933579',
              label: 'CONTAINS',
              depth: { min: 0, max: 1 },
              direction: 'TO',
              node: {},
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path_0 = (()-[id_1698231933579:CONTAINS*0..1]->())
        WHERE ALL(path_0_rel_id_1698231933579 in id_1698231933579 WHERE (path_0_rel_id_1698231933579.unitPrice < 10))
        RETURN * LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with count logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      return: ['*'],
      logic: ['>', ['Count', '@p1'], '1'],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
            relation: {
              label: 'DIRECTED',
              direction: 'TO',
              depth: { min: 1, max: 1 },
              node: {
                label: 'Movie',
                id: 'm1',
              },
            },
          },
        },
      ],
      limit: 5000,
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person)-[:DIRECTED*1..1]->(m1:Movie))
        WITH count(p1) AS p1_count, m1
        MATCH path1 = ((p1:Person)-[:DIRECTED*1..1]->(m1:Movie))
        WHERE (p1_count > 1)
        RETURN * LIMIT 5000`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with empty relation', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      query: [
        {
          id: 'path_0',
          node: {
            label: 'Movie',
            id: 'id_1730483610947',
            relation: {
              label: '',
              id: '',
              depth: {
                min: 0,
                max: 0,
              },
            },
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path_0 = ((id_1730483610947:Movie))
        RETURN * LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with upper case logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      query: [
        {
          id: 'path_0',
          node: {
            id: 'id_1731428699410',
            label: 'Character',
          },
        },
      ],
      logic: ['Upper', '@id_1731428699410.name'],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path_0 = ((id_1731428699410:Character))
        WHERE toUpper(id_1731428699410.name)
        RETURN *
        LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with boolean logic in relation', () => {
    const query: BackendQueryFormat = {
      saveStateID: '31966faf-0741-4a70-b92b-62c4b5e25d91',
      query: [
        {
          id: 'path_0',
          node: {
            id: 'id_1737115397734',
            label: 'Product',
            relation: {
              id: 'id_1737115397848',
              label: 'PRODUCT_MATERIAL',
              depth: {
                max: 1,
                min: 1,
              },
              direction: 'TO',
              node: {
                id: 'id_1737115397423',
                label: 'Material',
              },
            },
          },
        },
        {
          id: 'path_1',
          node: {
            id: 'id_1737115397734',
            label: 'Product',
            relation: {
              id: 'id_1737115611667',
              label: 'DEPARTS_FROM',
              depth: {
                max: 1,
                min: 1,
              },
              direction: 'TO',
              node: {
                id: 'id_1737115612952',
                label: 'Country',
                relation: {
                  id: 'id_1737115886717',
                  label: 'CountryFlow',
                  depth: {
                    max: 1,
                    min: 1,
                  },
                  direction: 'TO',
                  node: {
                    id: 'id_1737115684010',
                    label: 'Country',
                  },
                },
              },
            },
          },
        },
        {
          id: 'path_2',
          node: {
            id: 'id_1737115397734',
            label: 'Product',
            relation: {
              id: 'id_1737115682374',
              label: 'ARRIVES_AT',
              depth: {
                max: 1,
                min: 1,
              },
              direction: 'TO',
              node: {
                id: 'id_1737115684010',
                label: 'Country',
              },
            },
          },
        },
      ],
      machineLearning: [],
      limit: 100,
      return: ['*'],
      cached: false,
      logic: ['And', ['==', '@id_1737115397423.GO', '"852349"'], ['==', '@id_1737115886717.goodFlow', 'false']],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path_0 = ((id_1737115397734:Product)-[id_1737115397848:PRODUCT_MATERIAL*1..1]->(id_1737115397423:Material)) 
    MATCH path_1 = ((id_1737115397734:Product)-[id_1737115611667:DEPARTS_FROM*1..1]->(id_1737115612952:Country)-[id_1737115886717:CountryFlow*1..1]->(id_1737115684010:Country))
    MATCH path_2 = ((id_1737115397734:Product)-[id_1737115682374:ARRIVES_AT*1..1]->(id_1737115684010:Country))
    WHERE ALL(path_2_rel_id_1737115886717 in id_1737115886717 WHERE ((id_1737115397423.GO = "852349") and
    (path_2_rel_id_1737115886717.goodFlow = false))) RETURN * LIMIT 100`;

    expect(fixCypherSpaces(cypher.query)).toEqual(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with count', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'b1873956-e51b-46db-9c33-ab8bfbd177ef',
      query: [
        {
          id: 'path_0',
          node: {
            relation: {
              id: 'id_1739982191754',
              label: 'HAS',
              depth: {
                max: 1,
                min: 1,
              },
              direction: 'BOTH',
              node: {},
            },
          },
        },
      ],
      limit: 555,
      return: ['*'],
      cached: false,
    };

    const cypher = query2Cypher(query);
    const expectedCypher = 'MATCH path_0 = (()-[id_1739982191754:HAS*1..1]-()) RETURN * LIMIT 555';
    expect(fixCypherSpaces(cypher.query)).toEqual(fixCypherSpaces(expectedCypher));
    const expectedCypherCount =
      'MATCH path_0 = (()-[id_1739982191754:HAS*1..1]-()) RETURN COUNT(DISTINCT id_1739982191754) as id_1739982191754_count';
    expect(fixCypherSpaces(cypher.countQuery)).toEqual(fixCypherSpaces(expectedCypherCount));
  });

  it('should return correctly on a query with IN logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      logic: [StringFilterTypes.IN, '@p1.name', '"John, Doe"'],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person))
        WHERE (p1.name IN ["John", "Doe"])
        RETURN * LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });

  it('should return correctly on a query with NOT IN logic', () => {
    const query: BackendQueryFormat = {
      saveStateID: 'test',
      limit: 500,
      logic: [StringFilterTypes.NOT_IN, '@p1.name', '"John, Doe"'],
      query: [
        {
          id: 'path1',
          node: {
            label: 'Person',
            id: 'p1',
          },
        },
      ],
      return: ['*'],
    };

    const cypher = query2Cypher(query);
    const expectedCypher = `MATCH path1 = ((p1:Person))
        WHERE (NOT p1.name IN ["John", "Doe"])
        RETURN * LIMIT 500`;

    expect(fixCypherSpaces(cypher.query)).toBe(fixCypherSpaces(expectedCypher));
  });
});
