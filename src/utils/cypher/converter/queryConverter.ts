/*
 This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
 © Copyright Utrecht University(Department of Information and Computing Sciences)
*/

import type { BackendQueryFormat } from 'ts-common';
import { extractLogicCypher } from './logic';
import { extractExportCypher } from './export';
import type { QueryCacheData } from './model';
import { getNodeCypher } from './node';
import { log } from 'ts-common/src/logger/logger';

export type QueryCypher = {
  query: string;
  countQuery: string;
};

// formQuery uses the hierarchy to create cypher for each part of the query in the right order
export function query2Cypher(JSONQuery: BackendQueryFormat): QueryCypher {
  let totalQuery = '';
  let matchQuery = '';
  let cacheData: QueryCacheData = { entities: [], relations: [], unwinds: [] };

  // MATCH block
  for (const query of JSONQuery.query) {
    let match = 'MATCH ';
    const [nodeCypher, _cacheData] = getNodeCypher(query.node, cacheData);

    cacheData = _cacheData;
    for (let i = 0; i < cacheData.entities.length; i++) {
      cacheData.entities[i].queryId = query.id;
    }
    for (let i = 0; i < cacheData.relations.length; i++) {
      cacheData.relations[i].queryId = query.id;
    }
    // force contents of cacheData to be unique
    cacheData.entities = [...new Map(cacheData.entities.map(item => [item.id, item])).values()];
    cacheData.relations = [...new Map(cacheData.relations.map(item => [item.id, item])).values()];
    cacheData.unwinds = [...new Set(cacheData.unwinds)];

    // Generate cypher query for path
    if (query.id) {
      match += `${query.id} = (${nodeCypher})`;
    } else {
      match += nodeCypher;
    }

    // Exports (not used right now)
    const exports = extractExportCypher(query.node);

    if (exports.length > 0 && exports[0]) {
      match += ' WITH ' + exports.join(', ');
    }

    matchQuery = match.replace(/@/g, '').replace(/\\"/g, '"') + '\n';
    totalQuery += matchQuery;
  }

  // logic calculation for WHERE block
  if (JSONQuery.logic) {
    const { logic: _logic, where: whereLogic } = extractLogicCypher(JSONQuery.logic, cacheData);

    let logic = _logic as string;

    for (const relation of cacheData.relations) {
      // if we need to do logic on relations, this with is required
      if (relation.queryId && logic.includes(relation.id)) {
        logic = `ALL(${relation.queryId}_rel_${relation.id} in ${relation.id} WHERE ${logic.replace(
          relation.id,
          `${relation.queryId}_rel_${relation.id}`,
        )})`;
      }
    }
    let totalQueryWithLogic = totalQuery;
    if (whereLogic && whereLogic.size > 0) {
      totalQueryWithLogic += `WITH ${[...whereLogic].join(', ')}\n`;
      totalQuery = totalQueryWithLogic + totalQuery;
    }
    totalQuery += `WHERE ${logic}\n`;
  }

  let countQuery = totalQuery;

  // RETURN block
  if (JSONQuery.return.length === 0 || JSONQuery.return[0] === '*') {
    totalQuery += 'RETURN *';
  } else {
    totalQuery += 'RETURN ' + JSONQuery.return.join(', ').replace(/@/g, '');
  }

  // LIMIT block
  totalQuery += `\nLIMIT ${JSONQuery.limit}`;

  countQuery += 'RETURN ';
  countQuery += Object.values(cacheData.entities)
    .map(e => `COUNT(DISTINCT ${e.id}) as ${e.id}_count`)
    .join(', ');

  countQuery += Object.values(cacheData.entities).length > 0 && Object.values(cacheData.relations).length > 0 ? ', ' : '';
  countQuery += Object.values(cacheData.relations)
    .map(r => `COUNT(DISTINCT ${r.id}) as ${r.id}_count`)
    .join(', ');

  return { query: totalQuery, countQuery };
}
