import type { RelationStruct } from 'ts-common';
import { getNodeCypher } from './node';
import type { QueryCacheData } from './model';
import { log } from 'ts-common/src/logger/logger';

export const NoNodeError = new Error('relation must have a node');

export function getRelationCypher(JSONQuery: RelationStruct, cacheData: QueryCacheData): [string | undefined, QueryCacheData] {
  let label = '';

  if (JSONQuery.label) {
    label = `:${JSONQuery.label}`;
  }
  let id = '';
  if (JSONQuery.id) {
    id = JSONQuery.id;
  }
  let depth = '';

  if (JSONQuery.depth) {
    //  && JSONQuery.depth.min !== 1 && JSONQuery.depth.max !== 1
    depth = `*${JSONQuery.depth.min}..${JSONQuery.depth.max}`;
    cacheData.unwinds.push(id + '_unwind');
  }

  if (id) {
    cacheData.relations.push({ id, queryId: '' });
  }

  let cypher = `[${id}${label}${depth}]`;

  if (!JSONQuery.node) {
    return [undefined, cacheData];
  }
  const [nodeCypher, _cacheData] = getNodeCypher(JSONQuery.node, cacheData);

  if (JSONQuery.direction === 'TO') {
    cypher += `->${nodeCypher}`;
  } else if (JSONQuery.direction === 'FROM') {
    cypher += `-${nodeCypher}`;
  } else {
    cypher += `-${nodeCypher}`;
  }
  return [cypher, _cacheData];
}
