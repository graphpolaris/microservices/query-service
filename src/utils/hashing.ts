const FNV_OFFSET_BASIS = 2166136261;

export const hashDictionary = (dictionary: Record<"nodes" | "edges", string[]>): string => {
    // Convert the dictionary into a consistent string format
    const jsonString = JSON.stringify(dictionary, Object.keys(dictionary).sort());

    // FNV-1a hash implementation
    let hash = FNV_OFFSET_BASIS; // FNV offset basis
    for (let i = 0; i < jsonString.length; i++) {
        hash ^= jsonString.charCodeAt(i);
        hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24); // FNV prime
    }

    // Convert hash to a hexadecimal string
    return (hash >>> 0).toString(16); // Ensure unsigned integer representation
};

export const hashIsEqual = (hash1: string, hash2: string): boolean => {
    return hash1 === hash2;
};
