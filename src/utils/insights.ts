import { scaleOrdinal, scaleQuantize } from "d3";
import { JSDOM } from "jsdom";
import svg2img from "svg2img";
import "canvas";
import { visualizationColors, type GraphQueryResultFromBackend, type GraphQueryResultMetaFromBackend } from "ts-common";

import { type PlotType } from "plotly.js";

const dom = new JSDOM();

global.window = dom.window as any;
global.document = dom.window.document;
global.DocumentFragment = dom.window.DocumentFragment;
global.DOMParser = dom.window.DOMParser;
global.getComputedStyle = dom.window.getComputedStyle;
global.Element = dom.window.Element;
global.HTMLElement = dom.window.HTMLElement;

dom.window.HTMLCanvasElement.prototype.getContext = function () {
  return null;
};
dom.window.URL.createObjectURL = function () {
  return "";
};

export enum VariableType {
  statistic = "statistic",
  visualization = "visualization",
}

async function replaceAllAsync(string: string, regexp: RegExp, replacerFunction: CallableFunction) {
  const replacements = await Promise.all(Array.from(string.matchAll(regexp), (match) => replacerFunction(...match)));
  let i = 0;
  return string.replace(regexp, () => replacements[i++]);
}

export async function populateTemplate(html: string, result: GraphQueryResultMetaFromBackend, openVisualizationArray: any[]) {
  const regex = / *?{\{ *?(\w*?):([\w ]*?) *?\}\} *?/gm;

  return replaceAllAsync(html, regex, async (_: string, _type: string, name: string) => {
    const type = VariableType[_type as keyof typeof VariableType];

    switch (type) {
      case VariableType.statistic: {
        const [nodeType, feature, statistic] = name.split(" ");
        const node = result.metaData.nodes.types[nodeType];
        const attribute = node?.attributes[feature].statistics as any;
        if (attribute == null) return "";
        const value = attribute[statistic];
        return ` ${value} `;
      }

      case VariableType.visualization: {
        const activeVisualization = openVisualizationArray.find((x) => x.name == name); // TODO: enforce type

        if (!activeVisualization) {
          throw new Error("Tried to render non-existing visualization");
        }
        const xAxisData = getAttributeValues(result, activeVisualization.selectedEntity, activeVisualization.xAxisLabel!);
        let yAxisData: (string | number)[] = [];
        let zAxisData: (string | number)[] = [];

        if (activeVisualization.yAxisLabel != null) {
          yAxisData = getAttributeValues(result, activeVisualization.selectedEntity, activeVisualization.yAxisLabel);
        }

        if (activeVisualization.zAxisLabel != null) {
          zAxisData = getAttributeValues(result, activeVisualization.selectedEntity, activeVisualization.zAxisLabel);
        }

        const groupBy = activeVisualization.groupData;
        const stack = activeVisualization.stack;
        const showAxis = true;

        const xAxisLabel = "";
        const yAxisLabel = "";
        const zAxisLabel = "";

        const plotType = activeVisualization.plotType;

        const { plotData, layout } = preparePlotData(
          xAxisData,
          plotType,
          yAxisData,
          zAxisData,
          xAxisLabel,
          yAxisLabel,
          zAxisLabel,
          showAxis,
          groupBy,
          stack
        );

        const layout2 = {
          ...layout,
          width: 600,
          height: 400,
          title: activeVisualization.title,
        };

        const { newPlot } = await import("plotly.js");
        const plot = await newPlot(dom.window.document.createElement("div"), plotData, layout2);
        const svgString = plot.querySelector("svg")?.outerHTML;
        if (!svgString) {
          return "";
        }
        const dataURI = await svgToBase64(svgString);

        return `<img src="data:image/png;base64,${dataURI}" width="300" height="200" alt="${activeVisualization.title}" />`;
      }
    }
  });
}

const svgToBase64 = (svgString: string) => {
  return new Promise((resolve, reject) => {
    svg2img(svgString, (error: any, buffer: Buffer) => {
      if (error != null) reject(error);
      resolve(buffer.toString("base64"));
    });
  });
};

export const plotTypeOptions = ["bar", "scatter", "line", "histogram", "pie"] as const;
export type SupportedPlotType = (typeof plotTypeOptions)[number];

const groupByTime = (xAxisData: string[], groupBy: string, additionalVariableData?: (string | number)[]) => {
  // Function to parse the date-time string into a JavaScript Date object
  const parseDate = (dateStr: string) => {
    // Remove nanoseconds part and use just the standard "YYYY-MM-DD HH:MM:SS" part
    const cleanedDateStr = dateStr.split(".")[0];
    return new Date(cleanedDateStr);
  };

  // Grouping logic
  const groupedData = xAxisData.reduce((acc, dateStr, index) => {
    const date = parseDate(dateStr);
    let groupKey: string;

    if (groupBy === "yearly") {
      groupKey = date.getFullYear().toString(); // Group by year (e.g., "2012")
    } else if (groupBy === "quarterly") {
      const month = date.getMonth() + 1; // Adjust month for zero-indexed months
      const quarter = Math.floor((month - 1) / 3) + 1; // Calculate quarter (Q1-Q4)
      groupKey = `${date.getFullYear()}-Q${quarter}`;
    } else if (groupBy === "monthly") {
      // Group by month, e.g., "2012-07"
      groupKey = `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, "0")}`;
    } else {
      // Default case: group by year (or some other grouping logic)
      groupKey = date.getFullYear().toString();
    }

    // Initialize the group if it doesn't exist
    if (!acc[groupKey]) {
      acc[groupKey] = additionalVariableData
        ? typeof additionalVariableData[0] === "number"
          ? 0 // Initialize sum for numbers
          : [] // Initialize array for strings
        : 0; // Initialize count for no additional data
    }

    // Aggregate additional variable if provided
    if (additionalVariableData) {
      if (typeof additionalVariableData[index] === "number") {
        acc[groupKey] = (acc[groupKey] as number) + (additionalVariableData[index] as number);
      } else if (typeof additionalVariableData[index] === "string") {
        acc[groupKey] = [...(acc[groupKey] as string[]), additionalVariableData[index] as string];
      }
    } else {
      // Increment the count if no additionalVariableData
      acc[groupKey] = (acc[groupKey] as number) + 1;
    }

    return acc;
  }, {} as Record<string, number | string[]>);

  // Extract grouped data into arrays for Plotly
  const xValuesGrouped = Object.keys(groupedData);
  const yValuesGrouped = Object.values(groupedData);

  return { xValuesGrouped, yValuesGrouped };
};

const computeStringTickValues = (xValues: any[], maxTicks: number, maxLabelLength: number): any[] => {
  const truncatedValues = xValues.map((label) => (label.length > maxLabelLength ? `${label.slice(0, maxLabelLength)}…` : label));

  return truncatedValues;
};

export const preparePlotData = (
  xAxisData: (string | number)[],
  plotType: SupportedPlotType,
  yAxisData?: (string | number)[],
  zAxisData?: (string | number)[],
  xAxisLabel?: string,
  yAxisLabel?: string,
  zAxisLabel?: string,
  showAxis = true,
  groupBy?: string,
  stack?: boolean
): { plotData: Partial<Plotly.PlotData>[]; layout: Partial<Plotly.Layout> } => {
  const primaryColor = "#a2aab9"; // '--clr-sec--400'
  const lengthLabelsX = 7; // !TODO computed number of elements based
  const lengthLabelsY = 8; // !TODO computed number of elements based
  const mainColors = visualizationColors.GPCat.colors[14];

  const sharedTickFont = {
    family: "monospace",
    size: 12,
    color: "#374151", // !TODO get GP value
  };

  let xValues: (string | number)[] = [];
  let yValues: (string | number)[] = [];

  let colorScale: any;
  let colorDataZ: string[] = [];
  let colorbar: any = {};

  if (zAxisData && zAxisData.length > 0 && typeof zAxisData[0] === "number") {
    const mainColorsSeq = visualizationColors.GPSeq.colors[9];
    const numericZAxisData = zAxisData.filter((item): item is number => typeof item === "number");
    const zMin = numericZAxisData.reduce((min, val) => (val < min ? val : min), zAxisData[0]);
    const zMax = numericZAxisData.reduce((max, val) => (val > max ? val : max), zAxisData[0]);

    // !TODO: option to have a linear or quantize scale
    colorScale = scaleQuantize<string>().domain([zMin, zMax]).range(mainColorsSeq);

    colorDataZ = zAxisData?.map((item) => colorScale(item) || primaryColor);

    colorbar = {
      title: "Color Legend",
      tickvals: [zMin, zMax],
      ticktext: [`${zMin}`, `${zMax}`],
    };
  } else {
    const uniqueZAxisData = Array.from(new Set(zAxisData));

    if (zAxisData && uniqueZAxisData) {
      colorScale = scaleOrdinal<string>().domain(uniqueZAxisData.map(String)).range(mainColors);

      colorDataZ = zAxisData?.map((item) => colorScale(String(item)) || primaryColor);
      const sortedDomain = uniqueZAxisData.sort();
      colorbar = {
        title: "Color Legend",
        tickvals: sortedDomain,
        ticktext: sortedDomain.map((val) => String(val)),
        tickmode: "array",
      };
    }
  }

  if (!groupBy) {
    if (xAxisData.length !== 0 && yAxisData && yAxisData.length !== 0) {
      xValues = xAxisData;
      yValues = yAxisData;
    } else if (xAxisData.length !== 0 && (!yAxisData || yAxisData.length === 0)) {
      xValues = xAxisData;
      yValues = xAxisData.map((_, index) => index + 1);
    } else if (xAxisData.length === 0 && yAxisData && yAxisData.length !== 0) {
      xValues = yAxisData.map((_, index) => index + 1);
      yValues = yAxisData;
    }
  } else {
    if (groupBy) {
      if (yAxisData && yAxisData.length !== 0) {
        const { xValuesGrouped, yValuesGrouped } = groupByTime(xAxisData as string[], groupBy, yAxisData);
        xValues = xValuesGrouped;
        yValues = yValuesGrouped.flat();
      } else {
        const { xValuesGrouped, yValuesGrouped } = groupByTime(xAxisData as string[], groupBy);
        xValues = xValuesGrouped;
        yValues = yValuesGrouped.flat();
      }
    } else {
      xValues = xAxisData;
      yValues = xAxisData.map((_, index) => index + 1);
    }
  }

  let sortedLabels: string[] = [];
  let sortedFrequencies = [];

  let truncatedXLabels: string[] = [];
  let truncatedYLabels: string[] = [];
  let yAxisRange: number[] = [];

  if (typeof xValues[0] === "string") {
    truncatedXLabels = computeStringTickValues(xValues, 2, lengthLabelsX);
  }

  if (typeof yValues[0] === "string" && (plotType === "scatter" || plotType === "line")) {
    truncatedYLabels = computeStringTickValues(yValues, 2, lengthLabelsY);
  }
  const plotData = (() => {
    switch (plotType) {
      case "bar":
        if (typeof xAxisData[0] === "string" && groupBy == undefined) {
          const frequencyMap = xAxisData.reduce((acc, item) => {
            acc[item] = (acc[item] || 0) + 1;
            return acc;
          }, {} as Record<string, number>);

          const sortedEntries = Object.entries(frequencyMap).sort((a, b) => b[1] - a[1]);

          sortedLabels = sortedEntries.map(([label]) => String(label));
          sortedFrequencies = sortedEntries.map(([, frequency]) => frequency);

          // !TODO: y ranges: max value showed is rounded, eg 54 -> 50
          // need to specify tickvales and ticktext

          const maxYValue = Math.max(...sortedFrequencies);
          yAxisRange = [0, maxYValue];

          return [
            {
              type: "bar" as PlotType,
              x: xValues,
              y: yValues,
              marker: {
                color: colorDataZ?.length != 0 ? colorDataZ : primaryColor,
              },
              customdata: sortedLabels,
              hovertemplate: "<b>%{customdata}</b>: %{y}<extra></extra>",
            },
          ];
        } else {
          return [
            {
              type: "bar" as PlotType,
              x: xValues,
              y: yValues,
              marker: { color: primaryColor },
              customdata: xValues,
              hovertemplate: "<b>%{customdata}</b>: %{y}<extra></extra>",
            },
          ];
        }

      case "scatter":
        return [
          {
            type: "scatter" as PlotType,
            x: xValues,
            y: yValues,
            mode: "markers" as const,
            marker: {
              color: zAxisData && zAxisData.length > 0 ? colorDataZ : primaryColor,
              size: 7,
              stroke: 1,
            },
            customdata:
              xValues.length === 0
                ? yValues.map((y) => `Y: ${y}`)
                : yValues.length === 0
                ? xValues.map((x) => `X: ${x}`)
                : xValues.map((x, index) => {
                    const zValue = zAxisData && zAxisData.length > 0 ? zAxisData[index] : null;
                    return zValue ? `X: ${x} | Y: ${yValues[index]} | Color: ${zValue}` : `X: ${x} | Y: ${yValues[index]}`;
                  }),
            hovertemplate: "<b>%{customdata}</b><extra></extra>",
          },
        ];
      case "line":
        return [
          {
            type: "scatter" as PlotType,
            x: xValues,
            y: yValues,
            mode: "lines" as const,
            line: { color: primaryColor },
            customdata: xValues.map((label) => (label === "undefined" || label === "null" || label === "" ? "nonData" : "")),
            hovertemplate: "<b>%{customdata}</b><extra></extra>",
          },
        ];
      case "histogram":
        if (typeof xAxisData[0] === "string") {
          if (zAxisData && zAxisData?.length > 0) {
            const frequencyMap = xAxisData.reduce(
              (acc, item, index) => {
                const color = zAxisData ? colorScale(zAxisData[index]) : primaryColor;

                if (!acc[item]) {
                  acc[item] = {
                    count: 0,
                    colors: [],
                    zValues: [],
                    zValueCounts: {},
                  };
                }

                acc[item].count++;
                acc[item].colors.push(color);
                acc[item].zValues.push(zAxisData[index].toString());
                // Group and count zValues
                const zValue = zAxisData[index] || "(Empty)";
                acc[item].zValueCounts[zValue] = (acc[item].zValueCounts[zValue] || 0) + 1;

                return acc;
              },
              {} as Record<
                string,
                {
                  count: number;
                  colors: string[];
                  zValues: string[];
                  zValueCounts: Record<string, number>; // To store grouped counts
                }
              >
            );
            const colorToLegendName = new Map();
            const sortedCategories = Object.entries(frequencyMap).sort((a, b) => b[1].count - a[1].count);

            const tracesByColor: Record<string, { x: string[]; y: number[] }> = {};

            sortedCategories.forEach(([label, { colors, zValues }]) => {
              colors.forEach((color, idx) => {
                const zValue = zValues[idx];

                if (!colorToLegendName.has(color)) {
                  colorToLegendName.set(color, zValue);
                }

                if (!tracesByColor[color]) {
                  tracesByColor[color] = { x: [], y: [] };
                }
                tracesByColor[color].x.push(label);
                tracesByColor[color].y.push(1);
              });
            });

            sortedLabels = sortedCategories.map((element) => element[0]);

            const traces = Array.from(colorToLegendName.entries()).map(([color, legendName]) => {
              const colorData = tracesByColor[color];
              const categoryCountMap: Record<string, number> = {};

              sortedLabels.forEach((label) => {
                categoryCountMap[label] = frequencyMap[label].count;
              });
              const yValues = colorData.x.map((label, idx) => {
                const totalCount = categoryCountMap[label];
                const countForColor = colorData.y[idx];
                return stack ? (countForColor / totalCount) * 100 : countForColor;
              });

              const customdata = colorData.x.map((label, idx) => {
                const colorTranslation = colorToLegendName.get(color) === " " ? "(Empty)" : colorToLegendName.get(color);
                const percentage = ((100 * frequencyMap[label].zValueCounts[colorTranslation]) / frequencyMap[label].count).toFixed(1);
                return [label, !stack ? frequencyMap[label]?.zValueCounts[colorTranslation] || 0 : percentage, colorTranslation || " "];
              });
              return {
                x: colorData.x,
                y: yValues,
                type: "bar" as PlotType,
                name: legendName,
                marker: { color: color },
                customdata: customdata,
                hovertemplate:
                  "<b>X: %{customdata[0]}</b><br>" + "<b>Y: %{customdata[1]}</b><br>" + "<b>Color: %{customdata[2]}</b><extra></extra>",
                ...(stack ? { stackgroup: "one" } : {}),
              };
            });

            return traces;
          } else {
            const frequencyMap = xAxisData.reduce((acc, item) => {
              acc[item] = (acc[item] || 0) + 1;
              return acc;
            }, {} as Record<string, number>);

            const sortedEntries = Object.entries(frequencyMap).sort((a, b) => b[1] - a[1]);

            sortedLabels = sortedEntries.map(([label]) => String(label));
            sortedFrequencies = sortedEntries.map(([, frequency]) => frequency);

            return [
              {
                type: "bar" as PlotType,
                x: sortedLabels,
                y: sortedFrequencies,
                marker: { color: primaryColor },
                customdata: sortedLabels,
                hovertemplate: "<b>%{customdata}</b>: %{y}<extra></extra>",
              },
            ];
          }
        } else {
          if (zAxisData && zAxisData?.length > 0) {
            const binCount = 20; // Number of bins (you can make this configurable)
            const numericXAxisData = xAxisData.map((val) => Number(val)).filter((val) => !isNaN(val));

            const xMin = numericXAxisData.reduce((min, val) => Math.min(min, val), Infinity);
            const xMax = numericXAxisData.reduce((max, val) => Math.max(max, val), -Infinity);

            const binSize = (xMax - xMin) / binCount;

            // Create bins
            const bins = Array.from({ length: binCount }, (_, i) => ({
              range: [xMin + i * binSize, xMin + (i + 1) * binSize],
              count: 0,
              zValueCounts: {} as Record<string, number>, // To track zAxisData counts per bin
            }));

            // Assign data points to bins
            numericXAxisData.forEach((xValue, index) => {
              const zValue = zAxisData ? zAxisData[index] || "(Empty)" : "(Empty)";
              const binIndex = Math.floor((xValue - xMin) / binSize);
              const bin = bins[Math.min(binIndex, bins.length - 1)]; // Ensure the last value falls into the final bin

              bin.count++;
              bin.zValueCounts[zValue] = (bin.zValueCounts[zValue] || 0) + 1;
            });

            const colorToLegendName = new Map();
            const tracesByColor: Record<string, { x: string[]; y: number[] }> = {};

            bins.forEach((bin, binIndex) => {
              const binLabel = `[${bin.range[0].toFixed(1)}, ${bin.range[1].toFixed(1)})`;

              Object.entries(bin.zValueCounts).forEach(([zValue, count]) => {
                const color = zAxisData ? colorScale(zValue) : primaryColor;

                if (!colorToLegendName.has(color)) {
                  colorToLegendName.set(color, zValue);
                }

                if (!tracesByColor[color]) {
                  tracesByColor[color] = { x: [], y: [] };
                }

                tracesByColor[color].x.push(binLabel);
                tracesByColor[color].y.push(stack ? (count / bin.count) * 100 : count);
              });
            });

            const traces = Array.from(colorToLegendName.entries()).map(([color, legendName]) => {
              const colorData = tracesByColor[color];
              const customdata = colorData.x.map((binLabel, idx) => {
                const countForColor = colorData.y[idx];
                const percentage = stack ? countForColor.toFixed(1) + "%" : countForColor.toFixed(0);
                return [binLabel, countForColor, percentage, legendName];
              });

              return {
                x: colorData.x,
                y: colorData.y,
                type: "bar" as PlotType,
                name: legendName,
                marker: { color },
                customdata,
                autobinx: true,
                hovertemplate:
                  "<b>Bin: %{customdata[0]}</b><br>" +
                  "<b>Count/Percentage: %{customdata[2]}</b><br>" +
                  "<b>Group: %{customdata[3]}</b><extra></extra>",
                ...(stack ? { stackgroup: "one" } : {}),
              };
            });

            return traces;
          } else {
            // No zAxisData, simple histogram logic
            return [
              {
                type: "histogram" as PlotType,
                x: xAxisData,
                marker: { color: primaryColor },
                customdata: xAxisData,
              },
            ];
          }
        }
      case "pie":
        return [
          {
            type: "pie" as PlotType,
            labels: xValues.map(String),
            values: xAxisData,
            marker: { colors: mainColors },
          },
        ];
      default:
        return [];
    }
  })();

  const layout: Partial<Plotly.Layout> = {
    barmode: "stack",
    xaxis: {
      title: {
        text: showAxis ? (xAxisLabel ? xAxisLabel : "") : "",
        standoff: 30,
      },
      tickfont: sharedTickFont,
      showgrid: false,
      visible: showAxis,
      ...(typeof xAxisData[0] === "string" || (plotType === "histogram" && sortedLabels.length > 0)
        ? { type: "category", categoryarray: sortedLabels, categoryorder: "array" }
        : {}),
      showline: true,
      zeroline: false,
      tickvals: typeof xValues[0] == "string" ? xValues : undefined,
      ticktext: typeof xValues[0] == "string" ? truncatedXLabels : undefined,
    },

    yaxis: {
      showgrid: false,
      visible: showAxis,
      showline: true,
      zeroline: false,
      tickfont: sharedTickFont,
      title: {
        text: showAxis ? (yAxisLabel ? yAxisLabel : "") : "",
        standoff: 30,
      },
      tickvals: typeof yValues[0] === "string" && (plotType === "scatter" || plotType === "line") ? yValues : undefined,
      ticktext: typeof yValues[0] === "string" && (plotType === "scatter" || plotType === "line") ? truncatedYLabels : undefined,
    },
    font: {
      family: "Inter",
      size: 12,
      color: "#374151",
    },
    hoverlabel: {
      bgcolor: "rgba(255, 255, 255, 0.8)",
      bordercolor: "rgba(0, 0, 0, 0.2)",
      font: {
        family: "monospace",
        size: 14,
        color: "#374151",
      },
    },
  };
  return { plotData, layout };
};

export const getAttributeValues = (
  query: GraphQueryResultFromBackend,
  selectedEntity: string,
  attributeKey: string | number | undefined
): any[] => {
  if (!selectedEntity || !attributeKey) {
    return [];
  }

  if (attributeKey == " ") {
    return [];
  }
  return query.nodes
    .filter((item) => item.label === selectedEntity)
    .map((item) => {
      // Check if the attribute exists, return its value if it does, or an empty string otherwise
      return item.attributes && attributeKey in item.attributes && item.attributes[attributeKey] != ""
        ? item.attributes[attributeKey]
        : "NoData";
    });
};
