import type { NodeKey, LexicalEditor, DOMExportOutput, SerializedLexicalNode, Spread, EditorConfig } from 'lexical';
import { DecoratorNode } from 'lexical';
import type { GraphQueryResultMetaFromBackend } from 'ts-common/src/model/webSocket';


export enum VariableType {
  statistic = 'statistic',
  visualization = 'visualization',
}

export type SerializedVariableNode = Spread<
  {
    name: string,
    variableType: VariableType
  },
  SerializedLexicalNode
>;

export class VariableNode extends DecoratorNode<HTMLElement> {
  __name: string;
  __variableType: VariableType;

  static getType(): string {
    return 'variable-node';
  }

  static clone(node: VariableNode): VariableNode {
    return new VariableNode(node.__name, node.__variableType, node.__key);
  }

  constructor(name: string, type: VariableType, key?: NodeKey) {
    super(key);
    this.__name = name;
    this.__variableType = type;
  }

  setName(name: string) {
    const self = this.getWritable();
    this.__name = name;
  }

  getName(): string {
    const self = this.getLatest();
    return self.__name;
  }

  getTextContent(): string {
    const self = this.getLatest();
    return `{{ ${self.__variableType}:${self.__name} }}`;
  }

  // Import and export

  exportJSON(): SerializedVariableNode {
    return {
      type: this.getType(),
      variableType: this.__variableType,
      name: this.__name,
      version: 1,
    };
  }

  static importJSON(jsonNode: SerializedVariableNode): VariableNode {
    const node = new VariableNode(jsonNode.name, jsonNode.variableType);
    return node;
  }

  // View

  createDOM(config: EditorConfig): HTMLElement {
    const div = document.createElement('span');
    div.className = 'variable-node';
    div.innerHTML = this.getTextContent();
    return div;
  }

  updateDOM(): false {
    return false;
  }

  exportDOM(editor: LexicalEditor): DOMExportOutput {
    const self = this.getLatest();
    return {
      element: self.createDOM({ namespace: '', theme: {} }),
    };
  }

  decorate(): HTMLElement {
    const self = this.getLatest();
    return self.createDOM({ namespace: '', theme: {} });
  }
}
