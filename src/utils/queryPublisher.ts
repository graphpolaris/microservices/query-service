import { wsReturnKey, type BackendMessageHeader, type MachineLearning, type ToMLMessage } from 'ts-common';
import { log } from '../logger';
import type { GraphQueryResultFromBackend, GraphQueryResultMetaFromBackend } from 'ts-common/src/model/webSocket/graphResult';
import type { RabbitMqBroker } from 'ts-common/rabbitMq';

export class QueryPublisher {
  private frontendPublisher: RabbitMqBroker;
  private mlPublisher: RabbitMqBroker;
  private routingKey?: string;
  private headers?: BackendMessageHeader;
  private queryID?: string;

  constructor(frontendPublisher: RabbitMqBroker, mlPublisher: RabbitMqBroker) {
    this.frontendPublisher = frontendPublisher;
    this.mlPublisher = mlPublisher;
  }

  withHeaders(headers?: BackendMessageHeader) {
    this.headers = headers;
    return this;
  }

  withRoutingKey(routingKey?: string) {
    this.routingKey = routingKey;
    return this;
  }

  withQueryID(queryID?: string) {
    this.queryID = queryID;
    return this;
  }

  publishStatusToFrontend(status: string) {
    if (!this.headers || !this.routingKey || !this.queryID) {
      throw new Error('Headers or RoutingKey or queryID not set');
    }

    this.frontendPublisher.publishMessageToFrontend(
      {
        type: wsReturnKey.queryStatusUpdate,
        callID: this.headers.callID,
        value: this.queryID,
        status: status,
      },
      this.routingKey,
      this.headers,
    );
  }

  publishErrorToFrontend(reason: string) {
    if (!this.headers || !this.routingKey || !this.queryID) {
      throw new Error('Headers or RoutingKey or queryID not set');
    }

    this.frontendPublisher.publishMessageToFrontend(
      {
        type: wsReturnKey.queryStatusError,
        callID: this.headers.callID,
        value: this.queryID,
        status: reason,
      },
      this.routingKey,
      this.headers,
    );
  }

  publishTranslationResultToFrontend(query: string) {
    if (!this.headers || !this.routingKey || !this.queryID) {
      throw new Error('Headers or RoutingKey or queryID not set');
    }

    this.frontendPublisher.publishMessageToFrontend(
      {
        type: wsReturnKey.queryStatusTranslationResult,
        callID: this.headers.callID,
        value: {
          result: query,
          queryID: this.headers.callID,
        },
        status: 'success',
      },
      this.routingKey,
      this.headers,
    );
  }

  publishResultToFrontend(result: GraphQueryResultMetaFromBackend) {
    if (!this.headers || !this.routingKey || !this.queryID) {
      throw new Error('Headers or RoutingKey or queryID not set');
    }

    this.frontendPublisher.publishMessageToFrontend(
      {
        type: wsReturnKey.queryStatusResult,
        callID: this.headers.callID,
        value: {
          result: {
            type: 'nodelink',
            payload: result,
          },
          queryID: this.headers.callID,
        },
        status: 'success',
      },
      this.routingKey,
      this.headers,
    );
  }

  publishMachineLearningRequest(result: GraphQueryResultFromBackend, mlAttributes: MachineLearning, headers: BackendMessageHeader) {
    if (!this.headers || !this.routingKey) {
      throw new Error('Headers or RoutingKey or queryID not set');
    }

    // FIXME: Change ML to use the same message format that the frontend uses
    const toMlResult = {
      nodes: result.nodes.map(node => ({ ...node, id: node._id })),
      edges: result.edges.map(edge => ({ ...edge, id: edge._id })),
    };

    const message: ToMLMessage = {
      queryID: headers.callID,
      type: 'query_result',
      value: {
        type: 'nodelink',
        payload: toMlResult,
      },
      mlAttributes: mlAttributes.parameters,
    };

    const queueName = mlType2Queue[mlAttributes.type];
    if (!queueName) {
      log.error('Invalid ML type:', mlAttributes, mlAttributes.type);
      throw new Error('Invalid ML type');
    }

    this.mlPublisher.publishMessage({
      message: JSON.stringify(message),
      queueName,
      routingKey: queueName,
      options: {
        headers: {
          ...headers,
          message: Buffer.from(JSON.stringify(headers.message)),
        },
      },
    });

    log.debug('Published ML request:', queueName, headers.callID);
  }
}

const mlType2Queue = {
  centrality: 'ctr_queue',
  linkPrediction: 'lpr_queue',
  communityDetection: 'cdt_queue',
  shortestPath: 'stp_queue',
};
