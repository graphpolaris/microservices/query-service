import { UMSApi } from 'ts-common';
import { RabbitMqConnection } from 'ts-common/rabbitMq';
import nodemailer from 'nodemailer';
import { RedisConnector } from 'ts-common/redis';

export type QueryExecutionTypes = 'neo4j';

export const CACHE_KEY_PREFIX = Bun.env.CACHE_KEY_PREFIX || 'cached-schemas';
export const CACHE_DURATION = Bun.env.CACHE_DURATION ? parseInt(Bun.env.CACHE_DURATION) : 5 * 60 * 1000;
export const USER_MANAGEMENT_SERVICE_API = Bun.env.USER_MANAGEMENT_SERVICE_API || 'http://localhost:8000';
export const DEV = Bun.env.DEV || false;
export const ENV = Bun.env.ENV || 'develop';
export const RABBIT_USER = Bun.env.RABBIT_USER || 'rabbitmq';
export const RABBIT_PASSWORD = Bun.env.RABBIT_PASSWORD || 'DevOnlyPass';
export const RABBIT_HOST = Bun.env.RABBIT_HOST || 'localhost';
export const RABBIT_PORT = parseInt(Bun.env.RABBIT_PORT || '5672');
export const LOG_MESSAGES = Bun.env.LOG_MESSAGES || false;
export const LOG_LEVEL = parseInt(Bun.env.LOG_LEVEL || '-1');
export const REDIS_HOST = Bun.env.REDIS_HOST || 'localhost';
export const REDIS_PORT = parseInt(Bun.env.REDIS_PORT || '6379');
export const REDIS_PASSWORD = Bun.env.REDIS_PASSWORD || 'DevOnlyPass';
export const REDIS_SCHEMA_CACHE_DURATION = Bun.env.REDIS_SCHEMA_CACHE_DURATION || '60m';
export const QUERY_CACHE_DURATION =
  Bun.env.QUERY_CACHE_DURATION === '' || !Bun.env.QUERY_CACHE_DURATION ? '' : parseInt(Bun.env.QUERY_CACHE_DURATION);

export const redis = new RedisConnector(REDIS_PASSWORD, REDIS_HOST, REDIS_PORT);

export const ums = new UMSApi(USER_MANAGEMENT_SERVICE_API);
export const rabbitMq = new RabbitMqConnection({
  protocol: 'amqp',
  hostname: RABBIT_HOST,
  port: RABBIT_PORT,
  username: RABBIT_USER,
  password: RABBIT_PASSWORD,
});

export const SMTP_HOST = Bun.env.SMTP_HOST || '';
export const SMTP_PORT = Bun.env.SMTP_PORT || 587;
export const SMTP_USER = Bun.env.SMTP_USER || '';
export const SMTP_PASSWORD = Bun.env.SMTP_PASSWORD || '';
export const DEBUG_EMAIL = Bun.env.DEBUG_EMAIL !== 'false' || false;

export const mail =
  SMTP_HOST === '' || SMTP_USER === '' || SMTP_PASSWORD === ''
    ? undefined
    : nodemailer.createTransport(
        {
          // @ts-expect-error - ciphers is not in the type
          host: SMTP_HOST,
          port: SMTP_PORT,
          secure: false,
          tls: {
            ciphers: 'SSLv3',
            rejectUnauthorized: false,
          },
          auth: {
            user: SMTP_USER,
            pass: SMTP_PASSWORD,
          },
        },
        { debug: true, logger: true },
      );
